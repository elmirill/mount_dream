require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get landing" do
    get :landing
    assert_response :success
  end

  test "should get post_idea" do
    get :post_idea
    assert_response :success
  end

  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get give_feedback" do
    get :give_feedback
    assert_response :success
  end

end
