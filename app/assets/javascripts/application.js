// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require bootstrap-sprockets
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready(function() {
  
  //  Shrink navbar

  function shrinkNavbar() {
    if ($(this).scrollTop() > 0 || $(window).width() < 415) {
      $('.navbar').addClass('fixed');
      $('.navbar-brand').addClass('fixed');
      $('.navbar-nav li').addClass('fixed');
      $('.navbar-toggle').addClass('fixed');
    } else {
      $('.navbar').removeClass('fixed');
      $('.navbar-brand').removeClass('fixed');
      $('.navbar-nav li').removeClass('fixed');
      $('.navbar-toggle').removeClass('fixed');
    }
  }
  
  shrinkNavbar();
  
  $(window).scroll(function(){
    shrinkNavbar();
  });

  $(window).resize(function(){
    shrinkNavbar();
  });

  
  //// JS pseudo behavior
  
  // Hide form on submit
  
  $('.post-idea-form').find(':submit').click(function(event) {
    event.preventDefault();
    $('.post-idea-form').find('.form').remove();
    $('.post-idea-form').find('.well').removeClass('hidden');
  });
  
  
  // About page navbar
  
  $('.about-navbar .mission').click(function(event) {
    event.preventDefault();
    $('.about-tab').hide();
    $('.about-mission-wrapper').show();
    $('.about-navbar a').removeClass('active');
    $(this).addClass('active');
  });
  
  $('.about-navbar .what-is').click(function(event) {
    event.preventDefault();
    $('.about-tab').hide();
    $('.about-what-is-wrapper').show();
    $('.about-navbar a').removeClass('active');
    $(this).addClass('active');
  });
  
  $('.about-navbar .philosophy').click(function(event) {
    event.preventDefault();
    $('.about-tab').hide();
    $('.about-philosophy-wrapper').show();
    $('.about-navbar a').removeClass('active');
    $(this).addClass('active');
  });
  
  $('.about-navbar .culture').click(function(event) {
    event.preventDefault();
    $('.about-tab').hide();
    $('.about-culture-wrapper').show();
    $('.about-navbar a').removeClass('active');
    $(this).addClass('active');
  });
  
  $('.about-navbar .story').click(function(event) {
    event.preventDefault();
    $('.about-tab').hide();
    $('.about-story-wrapper').show();
    $('.about-navbar a').removeClass('active');
    $(this).addClass('active');
  });
  
  
  
// Help Center tabs toggle
  
  $('.faq-tab').find('h2').click(function() {
    $(this).parent('.faq-tab').find('.questions').slideToggle('fast');
    $(this).find('i').toggleClass('glyphicon-chevron-down');
    $(this).find('i').toggleClass('glyphicon-chevron-up');
  });
  
  setTimeout(function() {
		openFirstTab();
	}, 1000);
  
  function openFirstTab() {
    $('.questions:first').slideDown();
    $('.questions:first').parent().find('i').toggleClass('glyphicon-chevron-down');
    $('.questions:first').parent().find('i').toggleClass('glyphicon-chevron-up');
  }
  
  
  // Help Sticky help sidebar
  
    var sidebar = $('.help-center .sidebar nav');
    var sidebarWrapperHeight = $('.help-center .sidebar').height();
    var navbarHeight = 68;
    var marginTop = navbarHeight * 2;
    var staticMarginTop = sidebar.offset().top;
    var offtop = sidebar.offset().top - marginTop;
    
  function stickSidebar() {
    var footerHeight = $('.footer').height();
    var documentHeight = $('html').height();
    var marginBot = documentHeight - (staticMarginTop + footerHeight + sidebarWrapperHeight + 80);
    var offbtm = documentHeight - (footerHeight + sidebarWrapperHeight + marginTop + 80);
    var scrtop = $(window).scrollTop();
    
    if ($(window).width() > 767) {
      if (scrtop > offtop && sidebar.hasClass('static')) {
        sidebar.removeClass('static').addClass('fixed').css('top', marginTop);
      }
      if (scrtop < offtop && sidebar.hasClass('fixed')) {
        sidebar.removeClass('fixed').addClass('static').css('top', 'auto');
      }
      if (scrtop > offbtm && sidebar.hasClass('fixed')) {
        sidebar.removeClass('fixed').addClass('bottom').css('top', marginBot);
      }
      if (scrtop < offbtm && sidebar.hasClass('bottom')) {
        sidebar.removeClass('bottom').addClass('fixed').css('top', marginTop);
      }
    } else {
      sidebar.removeClass('fixed').addClass('static').css('top', 'auto');
    }
  }
  
  $(window).scroll(function(){
    stickSidebar();
  });
  $(window).resize(function(){
    stickSidebar();
  });
  
  
});