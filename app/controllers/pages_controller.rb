class PagesController < ApplicationController
  
  def home
  end

  def post_idea
  end

  def about
  end

  def give_feedback
  end
  
  def contact_customer_care
  end
  
  def help
  end
  
  def idea_safety
  end
  
  def tips_for_video
  end
  
  def before_you_post
  end
  
  def terms
  end
  
  def privacy
  end
  
end
