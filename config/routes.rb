Rails.application.routes.draw do
  
  root 'pages#home'
  
  get '/post-idea', to: 'pages#post_idea', as: :post_idea
  get '/about', to: 'pages#about', as: :about
  get '/give-feedback', to: 'pages#give_feedback', as: :give_feedback
  get '/login', to: 'pages#login', as: :login
  
  get '/help', to: 'pages#help', as: :help
  get '/help/idea-safety', to: 'pages#idea_safety', as: :idea_safety
  get '/help/tips-for-video-pitch', to: 'pages#tips_for_video', as: :tips_for_video
  get '/help/before-you-post', to: 'pages#before_you_post', as: :before_you_post
  
  get '/contact-customer-care', to: 'pages#contact_customer_care', as: :contact_customer_care
  get '/terms', to: 'pages#terms', as: :terms
  get '/privacy', to: 'pages#privacy', as: :privacy


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
